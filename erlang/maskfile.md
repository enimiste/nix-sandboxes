# Commands

## test

```sh
rebar3 eunit
```

### test watch

```sh
watchexec --clear --watch ./src --watch ./test -- $MASK test
```

## format

```sh
erlfmt --write ./rebar.config ./**/*.erl
```

## run

OPTIONS

- NAME
  - flags: --name
  - type: string

```sh
rebar3 escriptize >/dev/null

if [ -n "$NAME" ]; then
    _build/default/bin/mylib "$NAME"
else
    _build/default/bin/mylib
fi
```

## update

```bash
set -o errexit
set -o nounset
set -o pipefail

nix flake update
direnv exec . \
    devbox update
```

---

<!-- markdownlint-disable-next-line MD039 MD045 -->
This folder has been setup from the [`nix-sandboxes`'s template ![](https://img.shields.io/gitlab/stars/pinage404/nix-sandboxes?style=social)](https://gitlab.com/pinage404/nix-sandboxes)
