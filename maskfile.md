# Commands

## test

```sh
sh ./test_templates.sh
```

### test one_template (SANDBOX)

OPTIONS

* TEMP_DIR
  * flags: --temp-dir
  * type: string

```sh
sh ./test_templates.sh test_template "$SANDBOX" "$TEMP_DIR"
```

## list_languages

```sh
sh ./test_templates.sh list_templates
```

## warm_nix_cache

```bash
set -o errexit
set -o nounset
set -o pipefail

DIRENV_EXEC="direnv exec ."

for LANGUAGE in . $($MASK list_languages); do
    pushd "$LANGUAGE"
    direnv allow
    $DIRENV_EXEC true
    popd
done
```

## update

### update self

```bash
set -o errexit
set -o nounset
set -o pipefail

DIRENV_EXEC="direnv exec ."

nix flake update
$DIRENV_EXEC \
    devbox update
$DIRENV_EXEC \
    git-gamble --pass --message 'base: update dependencies' -- true || true
```

### update language (LANGUAGE)

```bash
set -o errexit
set -o nounset
set -o pipefail
set -o xtrace

DIRENV_EXEC="direnv exec ."
MESSAGE="$LANGUAGE: update dependencies"

pushd "$LANGUAGE"
if [ -f ".envrc" ]; then
    direnv allow
    if [ -f "maskfile.md" ]; then
        $DIRENV_EXEC \
            mask update
    fi
    $DIRENV_EXEC \
        git-gamble --pass --message "$MESSAGE" || git restore .
fi
```

### update all

```sh
set -o errexit
set -o nounset

git switch --create update
$MASK update self
for LANGUAGE in $($MASK list_languages); do
    $MASK update language "$LANGUAGE"
done
$MASK test
git switch -
git merge --no-edit update
git branch --delete update
```

## update_and_warm

```sh
set -o errexit

$MASK warm_nix_cache
$MASK update all
$MASK warm_nix_cache
```

## lint

```sh
set -o xtrace

for LANGUAGE in . $($MASK list_languages); do
    masklint run --maskfile "$LANGUAGE/maskfile.md"
done
```

---

<!-- markdownlint-disable-next-line MD039 MD045 -->
This folder has been setup from the [`nix-sandboxes`'s template ![](https://img.shields.io/gitlab/stars/pinage404/nix-sandboxes?style=social)](https://gitlab.com/pinage404/nix-sandboxes)
