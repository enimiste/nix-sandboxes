#!/usr/bin/env swipl

:- module(hello,[hello/2]).

hello(Expected, []) :-
    hello(Expected, ["world"]).

hello(Expected, [Name | _]) :-
    string_concat("Hello ", Name, Expected).

:- initialization(main, main).

main(Argv) :-
    hello(Expected, Argv),
    writeln(Expected).
