# Commands

## test

```sh
clojure -M:test-clj
```

### test watch

```sh
watchexec --clear -- $MASK test
```

## lint

```bash
set -o errexit
set -o nounset
set -o pipefail

clj-kondo --lint ./src/ ./test/ --fail-level error

clojure -M:test:eastwood

clojure -M:splint ./src/ ./test/
```

## format

```sh
clojure -M:format
```

## run

OPTIONS

- NAME
  - flags: --name
  - type: string

```sh
if [ -n "$NAME" ]; then
    clojure -M:hello "$NAME"
else
    clojure -M:hello
fi
```

## update

```bash
set -o errexit
set -o nounset
set -o pipefail

nix flake update
direnv exec . \
    devbox update
```

---

<!-- markdownlint-disable-next-line MD039 MD045 -->
This folder has been setup from the [`nix-sandboxes`'s template ![](https://img.shields.io/gitlab/stars/pinage404/nix-sandboxes?style=social)](https://gitlab.com/pinage404/nix-sandboxes)
