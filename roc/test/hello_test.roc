package [] {}

hello : Str -> Str
hello = \name ->
    displayName =
        when name is
            "" -> "world"
            _ -> name
    "Hello $(displayName)!"

expect hello "" == "Hello world!"
expect hello "Jane" == "Hello Jane!"
