{
  description = "A basic flake with a shell and git-gamble";

  # broken on Fish
  # as a workaround, use `nix develop` the first time
  # https://github.com/direnv/direnv/issues/1022
  # nixConfig.extra-substituters = [
  #   "https://pinage404-nix-sandboxes.cachix.org"
  #   "https://git-gamble.cachix.org"
  # ];
  # nixConfig.extra-trusted-public-keys = [
  #   "pinage404-nix-sandboxes.cachix.org-1:5zGRK2Ou+C27E7AdlYo/s4pow/w39afir+KRz9iWsZA="
  #   "git-gamble.cachix.org-1:afbVJAcYMKSs3//uXw3HFdyKLV66/KvI4sjehkdMM/I="
  # ];

  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";

  inputs.flake-utils.url = "github:numtide/flake-utils";

  inputs.masklint.url = "github:brumhard/masklint";
  inputs.masklint.inputs.nixpkgs.follows = "nixpkgs";
  inputs.masklint.inputs.utils.follows = "flake-utils";

  inputs.git-gamble.url = "gitlab:pinage404/git-gamble";
  inputs.git-gamble.inputs.masklint.follows = "masklint";

  outputs = { self, nixpkgs, flake-utils, masklint, git-gamble }:
    flake-utils.lib.eachDefaultSystem
      (system:
        let
          pkgs = nixpkgs.legacyPackages."${system}";
        in
        {
          devShells.default = pkgs.mkShellNoCC {
            packages = [
              pkgs.devbox
              masklint.packages."${system}".masklint
              git-gamble.packages."${system}".git-gamble
            ];
          };

          apps.default = self.apps."${system}".sandbox;
          apps.sandbox = {
            type = "app";
            program = "${self.packages."${system}".default}/bin/sandbox";
          };

          packages.default = self.packages."${system}".sandbox;
          packages.sandbox = pkgs.writeShellApplication {
            name = "sandbox";
            text = builtins.readFile ./use_sandbox.sh;
          };
          packages.sandboxes-name = pkgs.writeShellApplication {
            name = "sandboxes-name";
            text =
              let
                sandboxes-name = nixpkgs.lib.attrsets.mapAttrsToList (name: value: name) self.templates;
              in
              ''
                echo ${nixpkgs.lib.strings.concatStringsSep ":" sandboxes-name}
              '';
          };
          packages.sandboxes-list = pkgs.writeShellApplication {
            name = "sandboxes-list";
            text =
              let
                sandboxes-list = nixpkgs.lib.attrsets.mapAttrsToList (name: value: "\${BOLD}${name}\${NORMAL}: ${value.description}") self.templates;
              in
              ''
                BOLD=$(tput bold)
                NORMAL=$(tput sgr0)

                echo "${nixpkgs.lib.strings.concatStringsSep "\n" sandboxes-list}"
              '';
          };

          formatter = pkgs.nixpkgs-fmt;
        })
    //
    {
      templates = {
        c = {
          description = "C with formatting and test";
          path = ./c;
        };

        clojure = {
          description = "Clojure with formatting, linting and test";
          path = ./clojure;
        };

        common_lisp = {
          description = "Common Lisp with formatting and test";
          path = ./common_lisp;
        };

        cpp = {
          decription = "C++ with formatting and test";
          path = ./cpp;
        };

        elm = {
          description = "ELM with formatting, linting and test";
          path = ./elm;
        };

        erlang = {
          description = "Erlang with formatting and test";
          path = ./erlang;
        };

        forth = {
          description = "Forth with test";
          path = ./forth;
        };

        fsharp = {
          description = "F# with format, lint and test";
          path = ./fsharp;
        };

        haskell = {
          description = "Haskell with formatting, linting and test";
          path = ./haskell;
        };

        nix = {
          description = "Nix with formatting, linting and test";
          path = ./nix;
        };

        php = {
          description = "PHP with test";
          path = ./php;
        };

        prolog = {
          description = "Prolog with test";
          path = ./prolog;
        };

        python_poetry = {
          description = "Python with formatting, typing and test using Poetry";
          path = ./python_poetry;
        };

        roc = {
          description = "Roc lang with formatting and test";
          path = ./roc;
        };

        ruby = {
          description = "Ruby with test";
          path = ./ruby;
        };

        rust = {
          description = "Rust with formatting, linting and test";
          path = ./rust;
        };

        shell = {
          description = "Bash with formatting, linting and test";
          path = ./shell;
        };

        snapshot_testing = {
          description = "Small wrapper for snapshot testing";
          path = ./snapshot_testing;
        };

        typescript_bun = {
          description = "TypeScript with formatting and test on Bun";
          path = ./typescript_bun;
        };

        typescript_deno = {
          description = "TypeScript with formatting, linting and test on Deno";
          path = ./typescript_deno;
        };

        typescript_node_jest = {
          description = "TypeScript with formatting and test with Jest on NodeJS";
          path = ./typescript_node_jest;
        };

        typescript_node_vitest = {
          description = "TypeScript with formatting and test with Vitest on NodeJS";
          path = ./typescript_node_vitest;
        };
      };
    };
}
